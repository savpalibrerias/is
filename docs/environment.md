# is

Environment checks.



* * *

### is.chrome() 

Checks if the current browser is Chrome or Chromium. This method doesn't
support the `all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current browser is Chrome or Chromium.

**Example**:
```js
is.chrome();
```


### is.firefox() 

Checks if the current browser is Firefox. This method doesn't support the
`all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current browser is Firefox.

**Example**:
```js
is.firefox();
```


### is.edge() 

Checks if the current browser is Edge. This method doesn't support the
`all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current browser is Edge.

**Example**:
```js
is.firefox();
```


### is.ie(ver) 

Checks if the current browser is Internet Explorer. This method doesn't
support the `all` or `any` interfaces.

**Parameters**

**ver**: `Number`, The optional version number to check for.

**Returns**: `Boolean`, Whether the current browser is Internet Explorer.

**Example**:
```js
is.ie();
is.ie(9);
is.ie(10);
```


### is.opera() 

Checks if the current browser is Opera. This method doesn't support the
`all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current browser is Opera.

**Example**:
```js
is.opera();
```


### is.safari() 

Checks if the current browser is Safari. This method doesn't support the
`all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current browser is Safari.

**Example**:
```js
is.safari();
```


### is.vivaldi() 

Checks if the current browser is Vivaldi. This method doesn't support the
`all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current browser is Vivaldi.

**Example**:
```js
is.vivaldi();
```


### is.twitter() 

Checks if the current browser is Twitter's internal web view. This method
doesn't support the `all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current browser is Twitter's internal
webview.

**Example**:
```js
is.twitter();
```


### is.facebook() 

Checks if the current browser is Facebook's internal web view. This method
doesn't support the `all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current browser is Facebook's internal
webview.

**Example**:
```js
is.facebook();
```


### is.ios() 

Checks if the current device is runnig iOS. This method doesn't support the
`all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current device is runnig iOS.

**Example**:
```js
is.ios();
```


### is.iphone() 

Checks if the current device is an iPhone. This method doesn't support the
`all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current device is an iPhone.

**Example**:
```js
is.iphone();
```


### is.ipad() 

Checks if the current device is an iPad. This method doesn't support the
`all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current device is an iPad.

**Example**:
```js
is.ipad();
```


### is.ipod() 

Checks if the current device is an iPod. This method doesn't support the
`all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current device is an iPod.

**Example**:
```js
is.ipod();
```


### is.android() 

Checks if the current device is an Android device. This method doesn't
support the `all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current device is an Android device.

**Example**:
```js
is.android();
```


### is.androidPhone() 

Checks if the current device is an Android phone.This method doesn't
support the `all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current device is an Android phone.

**Example**:
```js
is.androidPhone();
```


### is.androidTablet() 

Checks if the current device is an Android tablet.This method doesn't
support the `all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current device is an Android tablet.

**Example**:
```js
is.androidTablet();
```


### is.blackberry() 

Checks if the current device is a Blackberry device. This method doesn't
support the `all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current device is a Blackberry device.

**Example**:
```js
is.blackberry();
```


### is.desktop() 

Checks if the current device is a desktop device.This method doesn't
support the `all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current device is a desktop device.

**Example**:
```js
is.desktop();
```


### is.linux() 

Checks if the current device running Linux (excluding Android). This method
doesn't support the `all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current device running Linux.

**Example**:
```js
is.linux();
```


### is.osx() 

Checks if the current device running OSX.This method doesn't support the
`all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current device running OSX.

**Example**:
```js
is.osx();
```


### is.mac() 

Checks if the current device running OSX (Mac).This method doesn't support
the `all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current device running OSX.

**Example**:
```js
is.mac();
```


### is.windows() 

Checks if the current device running Windows.This method doesn't support
the `all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current device running Windows.

**Example**:
```js
is.windows();
```


### is.windowsPhone() 

Checks if the current device is a Windows phone.This method doesn't support
the `all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current device is a Windows phone.

**Example**:
```js
is.windowsPhone();
```


### is.windowsTablet() 

Checks if the current device is a Windows tablet.This method doesn't
support the `all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current device is a Windows tablet.

**Example**:
```js
is.windowsTablet();
```


### is.mobile() 

Checks if the current device is a mobile device.This method doesn't support
the `all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current device is a mobile device.

**Example**:
```js
is.mobile();
```


### is.tablet() 

Checks if the current device is a tablet.This method doesn't support the
`all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current device is a tablet.

**Example**:
```js
is.tablet();
```


### is.online() 

Checks if the current device is on-line.This method doesn't support the
`all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current device is on-line.

**Example**:
```js
is.online();
```


### is.offline() 

Checks if the current device is off-line.This method doesn't support the
`all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current device is off-line.

**Example**:
```js
is.offline();
```


### is.touchDevice() 

Checks if the current device is touch capable.This method doesn't support
the `all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current device is touch capable.

**Example**:
```js
is.touchDevice();
```


### is.nodejs() 

Checks if the current environment is Node.js.This method doesn't support
the `all` or `any` interfaces.

**Returns**: `Boolean`, Whether the current environment is Node.js.

**Example**:
```js
is.nodejs();
```



* * *










