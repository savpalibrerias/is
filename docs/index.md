# is





* * *

### is.setRegexp(reg, key) 

Sets a custom regexp value for any existant regexp name.

**Parameters**

**reg**: `RegExp`, The regexp to use.

**key**: `String`, The regexp name to replace.



### is.setNamespace() 

Changes the namespace of the library to prevent name collissions.

**Returns**: `Object`, The 'is' object instance.



* * *










